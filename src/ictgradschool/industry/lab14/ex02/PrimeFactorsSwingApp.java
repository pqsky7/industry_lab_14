package ictgradschool.industry.lab14.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class PrimeFactorsSwingApp extends JPanel {

    public class PrimeFactorisationWorker extends SwingWorker<ArrayList<Long>, Void> {
        protected long number;

        public PrimeFactorisationWorker(long number) {
            this.number = number;
        }

        @Override
        protected ArrayList<Long> doInBackground(){
            ArrayList<Long> result = new ArrayList<>();
            for (long i = 2; i * i <= number; i++) {
                while (number % i == 0) {
                    result.add(i);
                    number = number / i;
                }
            }

            if (number > 1) {
                result.add(number);
            }
            return result;
        }

        protected void done(){
            ArrayList<Long> result = null;
            // Re-enable the Start button.
            _startBtn.setEnabled(true);
            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());

            try {
                result = get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            for(Long element: result){
                _factorValues.append(element + "\n");
            }

        }
    }

    private JButton _startBtn;        // Button to start the calculation process.
    private JTextArea _factorValues;  // Component to display the result.

    public PrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);

        _startBtn = new JButton("Compute");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                (new PrimeFactorisationWorker(n)).execute();
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new PrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

