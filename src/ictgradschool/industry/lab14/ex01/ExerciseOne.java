package ictgradschool.industry.lab14.ex01;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

/**
 * Created by qpen546 on 2/05/2017.
 */
public class ExerciseOne implements ActionListener {

    private JLabel progressLabel = null;
    private JLabel myLabel = null;
    private JButton myButton = null;

    /**
     * Called when the button is clicked.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        myButton.setEnabled(false);
// Start the SwingWorker running
        MySwingWorker worker = new MySwingWorker();
        try {
            worker.doInBackground();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    private class MySwingWorker extends SwingWorker<Integer, Void> {
        protected Integer doInBackground() throws Exception {
            int result = 0;
            for (int i = 0; i < 100; i++) {
                // Do some long-running stuff
                result += doStuffAndThings();
                // Report intermediate results
                progressLabel.setText("Progress: " + i + "%");
            }
            return result;
        }
        protected int doStuffAndThings(){return 0;}
        protected void done(){
            // When the SwingWorker has finished, display the result in myLabel.
            int result = 0;
            try {
                result = get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            myButton.setEnabled(true);
            myLabel.setText("Result: " + result);
        }
    }
}

