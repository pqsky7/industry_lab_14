package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;
    private Worker worker;

    public class Worker extends SwingWorker<Image,Void>{
        URL url;
        int width,height;

        public Worker(URL url,int width,int height) {
            this.url = url;
            this.width = width;
            this.height = height;
        }

        @Override
        protected Image doInBackground() throws Exception {
            try {
                 Image image = ImageIO.read(url);
                if (width == image.getWidth(null) && height == image.getHeight(null)) {
                   image = image;
                } else {
                    image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                    image.getWidth(null);
                }
                return image;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void done(){
            try {
                image = get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
    }


    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);
        worker = new Worker(url,width,height);
        worker.execute();
    }


    @Override
    public void paint(Painter painter) {
        if(worker.isDone()&&image!=null){
            painter.drawImage(this.image, fX, fY, fWidth, fHeight);
        }else {
            painter.setColor(Color.gray);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);
            painter.drawCenteredText("Loading",fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
    }
}
