package ictgradschool.industry.lab14.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class IntermediatePrimeFactorsSwingApp extends JPanel {

    public class PrimeFactorisationWorker extends SwingWorker<List<Long>, Long> {
        protected long number;

        public PrimeFactorisationWorker(long number) {
            this.number = number;
        }

        @Override
        protected List<Long> doInBackground() {
            List<Long> result = new ArrayList<>();
            for (long i = 2; i * i <= number; i++) {
                if (!isCancelled()) {
                    while (number % i == 0) {
                        result.add(i);
                        publish(i);
                        number = number / i;
                    }
                } else {
                    return null;
                }
            }

            if (number > 1) {
                result.add(number);
                publish(number);
            }
            return result;


        }


        @Override
        protected void process(List<Long> intermediateResult) {
            for (Long aLong : intermediateResult) {
                _factorValues.append(aLong + "\n");
            }
        }

        protected void done() {
            ArrayList<Long> result = null;
            // Re-enable the Start button.
            _startBtn.setEnabled(true);
            _endBtn.setEnabled(false);
            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());
        }
    }

    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _endBtn;
    private JTextArea _factorValues;  // Component to display the result.
    PrimeFactorisationWorker worker;

    public IntermediatePrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);


        _startBtn = new JButton("Compute");
        _endBtn = new JButton("Abort");
        _endBtn.setEnabled(false);
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);
                _endBtn.setEnabled(true);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                worker = new PrimeFactorisationWorker(n);
                worker.execute();
            }
        });

        _endBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

                // Disable the Start button until the result of the calculation is known.
                _endBtn.setEnabled(false);

                worker.cancel(true);
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_endBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new IntermediatePrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

